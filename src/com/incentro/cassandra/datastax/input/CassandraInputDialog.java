/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.input;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Props;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.TransPreviewFactory;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.ui.core.dialog.EnterNumberDialog;
import org.pentaho.di.ui.core.dialog.EnterTextDialog;
import org.pentaho.di.ui.core.dialog.PreviewRowsDialog;
import org.pentaho.di.ui.core.widget.StyledTextComp;
import org.pentaho.di.ui.core.widget.TextVar;
import org.pentaho.di.ui.spoon.job.JobGraph;
import org.pentaho.di.ui.trans.dialog.TransPreviewProgressDialog;
import org.pentaho.di.ui.trans.step.BaseStepDialog;
import org.pentaho.di.ui.trans.steps.tableinput.SQLValuesHighlight;

import com.incentro.cassandra.datastax.ConnectionCompression;

/**
 * Dialog class for the CassandraInput step
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class CassandraInputDialog extends BaseStepDialog implements StepDialogInterface {
	private static final Class<?> PKG = CassandraInputMeta.class;

	private Label wlCql;
	private StyledTextComp wtCql;

	private Label wlDatafrom;
	private CCombo wcDatafrom;
	private Listener lsDatafrom;

	private Label wlLimit;
	private TextVar wLimit;

	private Label wlEachRow;
	private Button wEachRow;

	private CassandraInputMeta meta;
	private boolean changedInDialog;

	private Label wlPosition;

	private Label wlHost;
	private TextVar wHost;

	private Label wlPort;
	private TextVar wPort;

	private Label wlUsername;
	private TextVar wUsername;

	private Label wlPassword;
	private TextVar wPassword;

	private Label wlWithSSL;
	private Button wWithSSL;

	private Label wlTruststorefile;
	private TextVar wTruststorefile;

	private Label wlCompression;
	private CCombo wCompression;

	private Label wlTruststorepass;
	private TextVar wTruststorepass;

	private Label wlKeyspace;
	private TextVar wKeyspace;

	public CassandraInputDialog(Shell parent, Object in, TransMeta transMeta, String sname) {
		super(parent, (BaseStepMeta) in, transMeta, sname);
		meta = (CassandraInputMeta) in;
	}

	public String open() {
		final Shell parent = getParent();
		final Display display = parent.getDisplay();
		FormData fd;

		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX | SWT.MIN);
		props.setLook(shell);
		setShellImage(shell, meta);

		ModifyListener lsMod = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				changedInDialog = false; // for prompting if dialog is simply closed
				meta.setChanged();
				if (e.widget instanceof TextVar) {
					TextVar t = (TextVar)e.widget;
					t.setToolTipText(transMeta.environmentSubstitute(t.getText()));
				}
			}
		};
		changed = meta.hasChanged();

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		shell.setLayout(formLayout);
		shell.setText(BaseMessages.getString(PKG, "DatastaxCassandraInput.StepName"));

		final int middle = props.getMiddlePct();
		final int margin = Const.MARGIN;

		// Stepname line
		wlStepname = new Label(shell, SWT.RIGHT);
		wlStepname.setText(BaseMessages.getString(PKG, "System.Label.StepName"));
		props.setLook(wlStepname);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(0, margin);
		wlStepname.setLayoutData(fd);
		wStepname = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wStepname.setText(stepname);
		props.setLook(wStepname);
		wStepname.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(100, 0);
		wStepname.setLayoutData(fd);

		wlHost = new Label(shell, SWT.RIGHT);
		wlHost.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Hostname.Label"));
		props.setLook(wlHost);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wStepname, margin);
		wlHost.setLayoutData(fd);

		wHost = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wHost.setText(meta.getCassadraNodes());
		props.setLook(wHost);
		wHost.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wStepname, margin);
		fd.right = new FormAttachment(100, 0);
		wHost.setLayoutData(fd);

		wlPort = new Label(shell, SWT.RIGHT);
		wlPort.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Port.Label"));
		props.setLook(wlPort);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wHost, margin);
		wlPort.setLayoutData(fd);

		wPort = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wPort.setText(meta.getCassandraPort());
		props.setLook(wPort);
		wPort.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wHost, margin);
		fd.right = new FormAttachment(100, 0);
		wPort.setLayoutData(fd);

		wlUsername = new Label(shell, SWT.RIGHT);
		wlUsername.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Username.Label"));
		props.setLook(wlUsername);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wPort, margin);
		wlUsername.setLayoutData(fd);

		wUsername = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wUsername.setText(meta.getUsername());
		props.setLook(wUsername);
		wUsername.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wPort, margin);
		fd.right = new FormAttachment(100, 0);
		wUsername.setLayoutData(fd);

		wlPassword = new Label(shell, SWT.RIGHT);
		wlPassword.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Password.Label"));
		props.setLook(wlPassword);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wUsername, margin);
		wlPassword.setLayoutData(fd);

		wPassword = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER | SWT.PASSWORD);
		wPassword.setText(meta.getPassword());
		props.setLook(wPassword);
		wPassword.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wUsername, margin);
		fd.right = new FormAttachment(100, 0);
		wPassword.setLayoutData(fd);

		wlWithSSL = new Label(shell, SWT.RIGHT);
		wlWithSSL.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.WithSSL.Label"));
		props.setLook(wlWithSSL);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wPassword, margin);
		wlWithSSL.setLayoutData(fd);

		wWithSSL = new Button(shell, SWT.CHECK);
		wWithSSL.setSelection(meta.getWithSSL());
		props.setLook(wWithSSL);
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wPassword, margin);
		fd.left = new FormAttachment(middle, 0);
		wWithSSL.setLayoutData(fd);

		// Trust store file line
		wlTruststorefile = new Label(shell, SWT.RIGHT);
		props.setLook(wlTruststorefile);
		wlTruststorefile.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.TrustStoreFile.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wWithSSL, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlTruststorefile.setLayoutData(fd);

		wTruststorefile = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wTruststorefile.setText(meta.getTrustStoreFilePath());
		props.setLook(wTruststorefile);
		wTruststorefile.addModifyListener(lsMod);
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wWithSSL, margin);
		fd.left = new FormAttachment(middle, 0);
		wTruststorefile.setLayoutData(fd);

		// Trust store password line
		wlTruststorepass = new Label(shell, SWT.RIGHT | SWT.PASSWORD);
		props.setLook(wlTruststorepass);
		wlTruststorepass.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.TrustStorePassword.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wTruststorefile, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlTruststorepass.setLayoutData(fd);

		wTruststorepass = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(wTruststorepass);
		wTruststorepass.setText(meta.getTrustStorePass());
		wTruststorepass.addModifyListener(lsMod);

		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wTruststorefile, margin);
		fd.left = new FormAttachment(middle, 0);
		wTruststorepass.setLayoutData(fd);

		// Compression
		wlCompression = new Label(shell, SWT.RIGHT);
		wlCompression.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Compression.Label"));
		wlCompression.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Compression.TipText"));
		props.setLook(wlCompression);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wTruststorepass, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlCompression.setLayoutData(fd);

		wCompression = new CCombo(shell, SWT.BORDER | SWT.DROP_DOWN);
		// set fixed compression methods
		wCompression.add(ConnectionCompression.SNAPPY.toString());
		wCompression.add(ConnectionCompression.NONE.toString());
		try {
			CassandraInputDialog.class.getClassLoader().loadClass("net.jpountz.lz4.LZ4Compressor");
			wCompression.add(ConnectionCompression.LZ4.toString());
		} catch (ClassNotFoundException e1) {
			// LZ4 not available
			wCompression.add(ConnectionCompression.LZ4.toString() + " (Not available)");
		}
		wCompression.setEditable(false);
		wCompression.addModifyListener(lsMod);
		props.setLook(wCompression);
		wCompression.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				switch (ConnectionCompression.fromString(wCompression.getText())) {
				case NONE:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.CompressionNone.TipText"));
					break;
				case SNAPPY:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.CompressionSnappy.TipText"));
					break;
				case LZ4:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.CompressionLZ4.TipText"));
					break;
				default:
					wCompression.setToolTipText(BaseMessages.getString(PKG,
							"DatastaxCassandraInputDialog.CompressionLZ4NotAvailable.TipText"));
				}
			}
		});
		wCompression.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wTruststorepass, margin);
		// fd.right = new FormAttachment(100, 0);
		wCompression.setLayoutData(fd);

		// Keyspace
		wlKeyspace = new Label(shell, SWT.RIGHT);
		wlKeyspace.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.SourceKeyspace.Label"));
		props.setLook(wlKeyspace);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wCompression, margin);
		wlKeyspace.setLayoutData(fd);

		wKeyspace = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wKeyspace.setText(meta.getKeyspace());
		props.setLook(wKeyspace);
		wKeyspace.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wCompression, margin);
		fd.right = new FormAttachment(100, 0);
		wKeyspace.setLayoutData(fd);

		// Some buttons
		wOK = new Button(shell, SWT.PUSH);
		wOK.setText(BaseMessages.getString(PKG, "System.Button.OK"));
		wPreview = new Button(shell, SWT.PUSH);
		wPreview.setText(BaseMessages.getString(PKG, "System.Button.Preview"));
		wCancel = new Button(shell, SWT.PUSH);
		wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel"));

		setButtonPositions(new Button[] { wOK, wPreview, wCancel }, margin, null);

		// Limit input ...
		wlLimit = new Label(shell, SWT.RIGHT);
		wlLimit.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.LimitSize"));
		props.setLook(wlLimit);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.bottom = new FormAttachment(wOK, -2 * margin);
		wlLimit.setLayoutData(fd);

		wLimit = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(wLimit);
		wLimit.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(wOK, -2 * margin);
		wLimit.setLayoutData(fd);

		// Execute for each row?
		wlEachRow = new Label(shell, SWT.RIGHT);
		wlEachRow.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.ExecuteForEachRow"));
		props.setLook(wlEachRow);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.bottom = new FormAttachment(wLimit, -margin);
		wlEachRow.setLayoutData(fd);

		wEachRow = new Button(shell, SWT.CHECK);
		props.setLook(wEachRow);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(wLimit, -margin);
		wEachRow.setLayoutData(fd);
		SelectionAdapter lsSelMod = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				meta.setChanged();
			}
		};
		wEachRow.addSelectionListener(lsSelMod);
		// TODO Implement wEachRow
		wEachRow.setEnabled(false);

		// Read date from...
		wlDatafrom = new Label(shell, SWT.RIGHT);
		wlDatafrom.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.InsertDataFromStep"));
		props.setLook(wlDatafrom);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.bottom = new FormAttachment(wEachRow, -margin);
		wlDatafrom.setLayoutData(fd);
		wcDatafrom = new CCombo(shell, SWT.BORDER);
		props.setLook(wcDatafrom);

		List<StepMeta> previousSteps = transMeta.findPreviousSteps(transMeta.findStep(stepname));
		for (StepMeta stepMeta : previousSteps) {
			wcDatafrom.add(stepMeta.getName());
		}

		wcDatafrom.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(wEachRow, -margin);
		wcDatafrom.setLayoutData(fd);
		wcDatafrom.setEnabled(false);

		wlPosition = new Label(shell, SWT.NONE);
		props.setLook(wlPosition);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(wlDatafrom, -margin);
		wlPosition.setLayoutData(fd);

		// Table line...
		wlCql = new Label(shell, SWT.NONE);
		wlCql.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.CQL"));
		props.setLook(wlCql);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wKeyspace, margin * 2);
		wlCql.setLayoutData(fd);

		wtCql = new StyledTextComp(transMeta, shell, SWT.MULTI | SWT.LEFT | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL, "");
		props.setLook(wtCql, Props.WIDGET_STYLE_FIXED);
		wtCql.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wlCql, margin);
		fd.right = new FormAttachment(100, -2 * margin);
		fd.bottom = new FormAttachment(wlPosition, -margin);
		wtCql.setLayoutData(fd);
		wtCql.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				setCQLToolTip();
				setPosition();
			}
		});

		wtCql.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				setPosition();
			}

			public void keyReleased(KeyEvent e) {
				setPosition();
			}
		});
		wtCql.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				setPosition();
			}

			public void focusLost(FocusEvent e) {
				setPosition();
			}
		});
		wtCql.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent e) {
				setPosition();
			}

			public void mouseDown(MouseEvent e) {
				setPosition();
			}

			public void mouseUp(MouseEvent e) {
				setPosition();
			}
		});

		// Text Higlighting
		wtCql.addLineStyleListener(new SQLValuesHighlight());

		// Add listeners
		lsCancel = new Listener() {
			public void handleEvent(Event e) {
				cancel();
			}
		};
		lsPreview = new Listener() {
			public void handleEvent(Event e) {
				preview();
			}
		};
		lsOK = new Listener() {
			public void handleEvent(Event e) {
				ok();
			}
		};
		lsDatafrom = new Listener() {
			public void handleEvent(Event e) {
				setFlags();
			}
		};

		wCancel.addListener(SWT.Selection, lsCancel);
		wPreview.addListener(SWT.Selection, lsPreview);
		wOK.addListener(SWT.Selection, lsOK);
		wcDatafrom.addListener(SWT.Selection, lsDatafrom);
		wcDatafrom.addListener(SWT.FocusOut, lsDatafrom);

		lsDef = new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				ok();
			}
		};

		wStepname.addSelectionListener(lsDef);
		wLimit.addSelectionListener(lsDef);

		// Detect X or ALT-F4 or something that kills this window...
		shell.addShellListener(new ShellAdapter() {
			public void shellClosed(ShellEvent e) {
				checkCancel(e);
			}
		});

		// fill dialog
		getData();
		changedInDialog = false; // for prompting if dialog is simply closed
		meta.setChanged(changed);

		// Set the shell size, based upon previous time...
		setSize();

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return stepname;
	}

	public void setPosition() {

		String scr = wtCql.getText();
		int linenr = wtCql.getLineAtOffset(wtCql.getCaretOffset()) + 1;
		int posnr = wtCql.getCaretOffset();

		// Go back from position to last CR: how many positions?
		int colnr = 0;
		while (posnr > 0 && scr.charAt(posnr - 1) != '\n' && scr.charAt(posnr - 1) != '\r') {
			posnr--;
			colnr++;
		}
		wlPosition.setText(BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.Position.Label", String.valueOf(linenr), String.valueOf(colnr)));
	}

	protected void setCQLToolTip() {
		wtCql.setToolTipText(transMeta.environmentSubstitute(wtCql.getText()));
	}

	/**
	 * Copy information from the meta-data input to the dialog fields.
	 */
	protected void getData() {
		wCompression.setText(meta.getCompression().toString());

		if (!Const.isEmpty(meta.getCqlStatement())) {
			wtCql.setText(meta.getCqlStatement());
		}
		wLimit.setText(String.valueOf(meta.getRowLimit()));

		boolean hasInfo = !(meta.getStepIOMeta().getInfoStreams() == null || meta.getStepIOMeta().getInfoStreams().isEmpty());
		wEachRow.setEnabled(hasInfo);
		wlEachRow.setEnabled(hasInfo);

		setCQLToolTip();
		setFlags();

		wStepname.selectAll();
		wStepname.setFocus();
	}

	private void checkCancel(ShellEvent e) {
		if (changedInDialog) {
			int save = JobGraph.showChangedWarning(shell, wStepname.getText());
			if (save == SWT.CANCEL) {
				e.doit = false;
			} else if (save == SWT.YES) {
				ok();
			} else {
				cancel();
			}
		} else {
			cancel();
		}
	}

	private void cancel() {
		stepname = null;
		meta.setChanged(changed);
		dispose();
	}

	private void getInfo(CassandraInputMeta meta, boolean preview) {
		meta.setCassadraNodes(wHost.getText());
		meta.setCassandraPort(wPort.getText());
		meta.setUsername(wUsername.getText());
		meta.setPassword(wPassword.getText());
		meta.setKeyspace(wKeyspace.getText());
		meta.setWithSSL(wWithSSL.getSelection());
		meta.setTrustStoreFilePath(wTruststorefile.getText());
		meta.setTrustStorePass(wTruststorepass.getText());
		meta.setCompression(ConnectionCompression.fromString(wCompression.getText()));
		meta.setCqlStatement(preview && !Const.isEmpty(wtCql.getSelectionText()) ? wtCql.getSelectionText() : wtCql.getText());
		meta.setRowLimit(Const.isEmpty(wLimit.getText()) ? 0 : Integer.parseInt(wLimit.getText()));
		meta.setExecuteForEachInputRow(wEachRow.getSelection());
	}

	private void ok() {
		if (Const.isEmpty(wStepname.getText())) {
			return;
		}

		stepname = wStepname.getText();
		getInfo(meta, false);

		dispose();
	}

	private void setFlags() {
		if (!Const.isEmpty(wcDatafrom.getText())) {
			// The foreach check box...
			wEachRow.setEnabled(true);
			wlEachRow.setEnabled(true);

			// The preview button...
			wPreview.setEnabled(false);
		} else {
			// The foreach check box...
			wEachRow.setEnabled(false);
			wEachRow.setSelection(false);
			wlEachRow.setEnabled(false);

			// The preview button...
			wPreview.setEnabled(true);
		}

	}

	/**
	 * Preview the data generated by this step. This generates a transformation
	 * using this step & a dummy and previews it.
	 *
	 */
	private void preview() {
		// Create the table input reader step...
		CassandraInputMeta oneMeta = new CassandraInputMeta();
		getInfo(oneMeta, true);

		TransMeta previewMeta = TransPreviewFactory.generatePreviewTransformation(transMeta, oneMeta,
				wStepname.getText());

		EnterNumberDialog numberDialog = new EnterNumberDialog(shell, props.getDefaultPreviewSize(),
				BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.EnterPreviewSize"),
				BaseMessages.getString(PKG, "DatastaxCassandraInputDialog.NumberOfRowsToPreview"));
		int previewSize = numberDialog.open();
		if (previewSize > 0) {
			TransPreviewProgressDialog progressDialog = new TransPreviewProgressDialog(shell, previewMeta,
					new String[] { wStepname.getText() }, new int[] { previewSize });
			progressDialog.open();

			Trans trans = progressDialog.getTrans();
			String loggingText = progressDialog.getLoggingText();

			if (!progressDialog.isCancelled()) {
				if (trans.getResult() != null && trans.getResult().getNrErrors() > 0) {
					EnterTextDialog etd = new EnterTextDialog(shell,
							BaseMessages.getString(PKG, "System.Dialog.PreviewError.Title"),
							BaseMessages.getString(PKG, "System.Dialog.PreviewError.Message"), loggingText, true);
					etd.setReadOnly();
					etd.open();
				} else {
					PreviewRowsDialog prd = new PreviewRowsDialog(shell, transMeta, SWT.NONE, wStepname.getText(),
							progressDialog.getPreviewRowsMeta(wStepname.getText()),
							progressDialog.getPreviewRows(wStepname.getText()), loggingText);
					prd.open();
				}
			}

		}
	}

}
