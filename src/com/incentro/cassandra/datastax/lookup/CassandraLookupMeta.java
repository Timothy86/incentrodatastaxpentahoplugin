/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.annotations.Step;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.encryption.Encr;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.metastore.api.IMetaStore;
import org.w3c.dom.Node;

import com.incentro.cassandra.datastax.ConnectionCompression;

@Step( id = "DatastaxCassandraLookup"
	 , image = "DatastaxCassandraLookup.svg"
	 , name = "DatastaxCassandraLookup.StepName"
	 , i18nPackageName = "com.incentro.cassandra.datastax.lookup.messages"
	 , description = "DatastaxCassandraLookup.StepDescription"
	 , categoryDescription = "DatastaxCassandraLookup.StepCategory"
	 , documentationUrl = "http://htmlpreview.github.io/?https://bitbucket.org/incentro-ondemand/incentrodatastaxpentahoplugin/raw/c7131c3dc7c28241d445301540c1e9768dfe0386/documentation/DatastaxCassandraLookup.html")
public class CassandraLookupMeta extends BaseStepMeta implements StepMetaInterface {

	public static final String[] conditionStrings = new String[] { "=", "<", "<=", ">", ">=", };

	protected String cassadraHost;

	protected String cassandraPort;

	protected String username;

	protected String password;

	protected String keyspace;

	protected String columnFamily;

	protected Boolean withSSL;

	protected String trustStoreFilePath;

	protected String trustStorePass;

	/** query compression method to use in the driver */
	protected ConnectionCompression compression;

	protected List<String> keyName;

	protected List<String> keyCondition;

	protected List<String> keyCompareFieldName;

	protected List<String> keyCompareValue;

	/** new name for value ... */
	protected List<String> returnValueName;

	/** new name for value ... */
	protected List<String> returnValueNewName;

	/** default value in case not found... */
	protected List<String> returnValueDefault;

	/** type of default value */
	protected List<Integer> returnValueDefaultType;

	@Override
	public String getXML() {
		StringBuffer retval = new StringBuffer();

		if (!Const.isEmpty(cassadraHost)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_host", cassadraHost));
		}

		if (!Const.isEmpty(cassandraPort)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_port", cassandraPort));
		}

		if (!Const.isEmpty(username)) {
			retval.append("    ").append(XMLHandler.addTagValue("username", username));
		}

		if (!Const.isEmpty(password)) {
			retval.append("    ").append(XMLHandler.addTagValue("password", Encr.encryptPasswordIfNotUsingVariables(password)));
		}

		if (!Const.isEmpty(keyspace)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_keyspace", keyspace));
		}

		if (!Const.isEmpty(columnFamily)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_columnFamily", columnFamily));
		}

		if (withSSL != null) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_withSSL", (withSSL ? "yes" : "no")));
		}

		if (!Const.isEmpty(trustStoreFilePath)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_trustStoreFilePath", trustStoreFilePath));
		}

		if (!Const.isEmpty(trustStorePass)) {
			retval.append("    ")
					.append(XMLHandler.addTagValue("cassandra_trustStorePass", Encr.encryptPasswordIfNotUsingVariables(trustStorePass)));
		}

		if (compression != null) {
			retval.append("    ").append(XMLHandler.addTagValue("compression", compression.toString()));
		}

		retval.append("    <keys>\n");
		for (int i = 0; i < keyName.size(); i++) {
			retval.append("      <key>\n");
			retval.append("        ").append(XMLHandler.addTagValue("keyName", keyName.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("condition", this.keyCondition.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("keyCompareFieldName", this.keyCompareFieldName.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("keyCompareValue", this.keyCompareValue.get(i)));
			retval.append("      </key>\n");
		}
		retval.append("    </keys>\n");

		retval.append("    <returnValues>\n");
		for (int i = 0; i < returnValueName.size(); i++) {
			retval.append("      <returnValue>\n");
			retval.append("        ").append(XMLHandler.addTagValue("returnValueName", returnValueName.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("returnValueNewName", this.returnValueNewName.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("returnValueDefault", this.returnValueDefault.get(i)));
			retval.append("        ").append(XMLHandler.addTagValue("returnValueDefaultType", this.returnValueDefaultType.get(i)));
			retval.append("      </returnValue>\n");
		}
		retval.append("    </returnValues>\n");

		return retval.toString();
	}

	@Override
	public void loadXML(Node stepnode, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleXMLException {
		cassadraHost = XMLHandler.getTagValue(stepnode, "cassandra_host");
		cassandraPort = XMLHandler.getTagValue(stepnode, "cassandra_port");
		username = XMLHandler.getTagValue(stepnode, "username");
		password = XMLHandler.getTagValue(stepnode, "password");
		if (!Const.isEmpty(password)) {
			password = Encr.decryptPasswordOptionallyEncrypted(password);
		}
		keyspace = XMLHandler.getTagValue(stepnode, "cassandra_keyspace");
		columnFamily = XMLHandler.getTagValue(stepnode, "cassandra_columnFamily");
		withSSL = (XMLHandler.getTagValue(stepnode, "cassandra_withSSL").equals("yes") ? true : false);
		trustStoreFilePath = XMLHandler.getTagValue(stepnode, "cassandra_trustStoreFilePath");
		trustStorePass = XMLHandler.getTagValue(stepnode, "cassandra_trustStorePass");
		if (!Const.isEmpty(trustStorePass)) {
			trustStorePass = Encr.decryptPasswordOptionallyEncrypted(trustStorePass);
		}
		String sCompression = XMLHandler.getTagValue(stepnode, "compression");

		keyName = new ArrayList<String>();
		keyCondition = new ArrayList<String>();
		keyCompareFieldName = new ArrayList<String>();
		keyCompareValue = new ArrayList<String>();
		Node keysNode = XMLHandler.getSubNode(stepnode, "keys");
		int nr = XMLHandler.countNodes(keysNode, "key");
		for (int i = 0; i < nr; i++) {
			Node keyNode = XMLHandler.getSubNodeByNr(keysNode, "key", i);
			if (XMLHandler.getSubNode(keyNode, "keyName") != null) {
				keyName.add(XMLHandler.getSubNode(keyNode, "keyName").getTextContent());

				Node subNode = XMLHandler.getSubNode(keyNode, "condition");
				if (subNode != null) {
					keyCondition.add(subNode.getTextContent());
					keyCompareFieldName.add(XMLHandler.getSubNode(keyNode, "keyCompareFieldName").getTextContent());
					keyCompareValue.add(XMLHandler.getSubNode(keyNode, "keyCompareValue").getTextContent());
				}
				else {
					// saved in older version
					keyCondition.add(XMLHandler.getSubNode(keyNode, "comparitor").getTextContent());
					keyCompareFieldName.add(XMLHandler.getSubNode(keyNode, "keyCompairFieldName").getTextContent());
					keyCompareValue.add(XMLHandler.getSubNode(keyNode, "keyCompairValue").getTextContent());
				}
			}
		}

		returnValueName = new ArrayList<String>();
		returnValueNewName = new ArrayList<String>();
		returnValueDefault = new ArrayList<String>();
		returnValueDefaultType = new ArrayList<Integer>();
		Node valuesNode = XMLHandler.getSubNode(stepnode, "returnValues");
		int valueNr = XMLHandler.countNodes(valuesNode, "returnValue");
		for (int i = 0; i < valueNr; i++) {
			Node valueNode = XMLHandler.getSubNodeByNr(valuesNode, "returnValue", i);
			if (XMLHandler.getSubNode(valueNode, "returnValueName") != null) {
				returnValueName.add(XMLHandler.getSubNode(valueNode, "returnValueName").getTextContent());
				returnValueNewName.add(XMLHandler.getSubNode(valueNode, "returnValueNewName").getTextContent());
				returnValueDefault.add(XMLHandler.getSubNode(valueNode, "returnValueDefault").getTextContent());
				returnValueDefaultType.add(Integer.parseInt(XMLHandler.getSubNode(valueNode, "returnValueDefaultType").getTextContent()));
			}
		}

		if (cassadraHost == null) {
			cassadraHost = "";
		}
		if (cassandraPort == null) {
			cassandraPort = "";
		}
		if (username == null) {
			username = "";
		}
		if (password == null) {
			password = "";
		}
		if (keyspace == null) {
			keyspace = "";
		}
		if (columnFamily == null) {
			columnFamily = "";
		}
		if (withSSL == null) {
			withSSL = false;
		}
		if (trustStoreFilePath == null) {
			trustStoreFilePath = "";
		}
		if (trustStorePass == null) {
			trustStorePass = "";
		}
		if (keyName == null) {
			keyName = new ArrayList<String>();
		}
		if (keyCondition == null) {
			keyCondition = new ArrayList<String>();
		}
		if (keyCompareFieldName == null) {
			keyCompareFieldName = new ArrayList<String>();
		}
		if (returnValueName == null) {
			returnValueName = new ArrayList<String>();
		}
		if (returnValueNewName == null) {
			returnValueNewName = new ArrayList<String>();
		}
		if (returnValueDefault == null) {
			returnValueDefault = new ArrayList<String>();
		}
		if (returnValueDefaultType == null) {
			returnValueDefaultType = new ArrayList<Integer>();
		}
		compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);
	}

	@Override
	public void readRep(Repository rep, ObjectId id_step, List<DatabaseMeta> databases, Map<String, Counter> counters)
			throws KettleException {
		cassadraHost = rep.getStepAttributeString(id_step, 0, "cassandra_host");
		cassandraPort = rep.getStepAttributeString(id_step, 0, "cassandra_port");
		username = rep.getStepAttributeString(id_step, 0, "username");
		password = rep.getStepAttributeString(id_step, 0, "password");
		if (!Const.isEmpty(password)) {
			password = Encr.decryptPasswordOptionallyEncrypted(password);
		}
		keyspace = rep.getStepAttributeString(id_step, 0, "cassandra_keyspace");
		columnFamily = rep.getStepAttributeString(id_step, 0, "cassandra_columnFamily");
		withSSL = rep.getStepAttributeString(id_step, 0, "withSSL").equals("yes") ? true : false;
		trustStoreFilePath = rep.getStepAttributeString(id_step, 0, "cassandra_trustStoreFilePath");
		trustStorePass = rep.getStepAttributeString(id_step, 0, "cassandra_trustStorePass");
		if (!Const.isEmpty(trustStorePass)) {
			trustStorePass = Encr.decryptPasswordOptionallyEncrypted(trustStorePass);
		}
		String sCompression = rep.getStepAttributeString(id_step, 0, "compression");

		keyName = new ArrayList<String>();
		keyCondition = new ArrayList<String>();
		keyCompareFieldName = new ArrayList<String>();
		keyCompareValue = new ArrayList<String>();
		int nrKeyName = rep.countNrStepAttributes(id_step, "keyName");
		for (int i = 0; i < nrKeyName; i++) {
			keyName.add(rep.getStepAttributeString(id_step, i, "keyName"));
			String condition = rep.getStepAttributeString(id_step, i, "condition");
			String fieldName = rep.getStepAttributeString(id_step, i, "keyCompareFieldName");
			String value = rep.getStepAttributeString(id_step, i, "keyCompareValue");
			if (Const.isEmpty(condition)) {
				// saved in older version
				condition = rep.getStepAttributeString(id_step, i, "comparitor");
				fieldName = rep.getStepAttributeString(id_step, i, "keyCompairFieldName");
				value = rep.getStepAttributeString(id_step, i, "keyCompairValue");
			}
			keyCondition.add(condition);
			keyCompareFieldName.add(fieldName);
			keyCompareValue.add(value);
		}

		returnValueName = new ArrayList<String>();
		returnValueNewName = new ArrayList<String>();
		returnValueDefault = new ArrayList<String>();
		returnValueDefaultType = new ArrayList<Integer>();
		int nrReturnValues = rep.countNrStepAttributes(id_step, "returnValueName");
		for (int i = 0; i < nrReturnValues; i++) {
			returnValueName.add(rep.getStepAttributeString(id_step, i, "returnValueName"));
			returnValueNewName.add(rep.getStepAttributeString(id_step, i, "returnValueNewName"));
			returnValueDefault.add(rep.getStepAttributeString(id_step, i, "returnValueDefault"));
			returnValueDefaultType.add(Integer.parseInt(rep.getStepAttributeString(id_step, i, "returnValueDefaultType")));
		}

		if (cassadraHost == null) {
			cassadraHost = "";
		}
		if (cassandraPort == null) {
			cassandraPort = "";
		}
		if (username == null) {
			username = "";
		}
		if (password == null) {
			password = "";
		}
		if (keyspace == null) {
			keyspace = "";
		}
		if (columnFamily == null) {
			columnFamily = "";
		}
		if (withSSL == null) {
			withSSL = false;
		}
		if (trustStoreFilePath == null) {
			trustStoreFilePath = "";
		}
		if (trustStorePass == null) {
			trustStorePass = "";
		}
		if (keyName == null) {
			keyName = new ArrayList<String>();
		}
		if (keyCondition == null) {
			keyCondition = new ArrayList<String>();
		}
		if (keyCompareFieldName == null) {
			keyCompareFieldName = new ArrayList<String>();
		}
		if (returnValueName == null) {
			returnValueName = new ArrayList<String>();
		}
		if (returnValueNewName == null) {
			returnValueNewName = new ArrayList<String>();
		}
		if (returnValueDefault == null) {
			returnValueDefault = new ArrayList<String>();
		}
		if (returnValueDefaultType == null) {
			returnValueDefaultType = new ArrayList<Integer>();
		}
		compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_transformation, ObjectId id_step) throws KettleException {
		if (!Const.isEmpty(cassadraHost)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_host", cassadraHost);
		}

		if (!Const.isEmpty(cassandraPort)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_port", cassandraPort);
		}

		if (!Const.isEmpty(username)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "username", username);
		}

		if (!Const.isEmpty(password)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "password", Encr.encryptPasswordIfNotUsingVariables(password));
		}

		if (!Const.isEmpty(keyspace)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_keyspace", keyspace);
		}

		if (!Const.isEmpty(columnFamily)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_columnFamily", columnFamily);
		}

		if (withSSL != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "withSSL", withSSL ? "yes" : "no");
		}

		if (!Const.isEmpty(trustStoreFilePath)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_trustStoreFilePath", trustStoreFilePath);
		}

		if (!Const.isEmpty(trustStorePass)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_trustStorePass",
					Encr.encryptPasswordIfNotUsingVariables(trustStorePass));
		}
		if (compression != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "compression", compression.toString());
		}
		for (int i = 0; i < keyName.size(); i++) {
			rep.saveStepAttribute(id_transformation, id_step, i, "keyName", keyName.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "condition", keyCondition.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "keyCompareFieldName", keyCompareFieldName.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "keyCompareValue", keyCompareValue.get(i));
		}
		for (int i = 0; i < returnValueName.size(); i++) {
			rep.saveStepAttribute(id_transformation, id_step, i, "returnValueName", returnValueName.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "returnValueNewName", returnValueNewName.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "returnValueDefault", returnValueDefault.get(i));
			rep.saveStepAttribute(id_transformation, id_step, i, "returnValueDefaultType", returnValueDefaultType.get(i));
		}
	}

	@Override
	public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta, Trans trans) {
		return new CassandraLookup(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	@Override
	public StepDataInterface getStepData() {
		return new CassandraLookupData();
	}

	@Override
	public String getDialogClassName() {
		return CassandraLookupDialog.class.getName();
	}

	public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta, TransMeta transMeta, String name) {
		return new CassandraLookupDialog(shell, meta, transMeta, name);
	}

	@Override
	public void setDefault() {
		cassadraHost = "localhost";
		cassandraPort = "9042";
		username = "";
		password = "";
		keyspace = "";
		columnFamily = "";
		withSSL = false;
		trustStoreFilePath = "";
		trustStorePass = "";
		compression = ConnectionCompression.SNAPPY;
		keyName = new ArrayList<String>();
		keyCondition = new ArrayList<String>();
		keyCompareFieldName = new ArrayList<String>();
		keyCompareValue = new ArrayList<String>();
		returnValueName = new ArrayList<String>();
		returnValueNewName = new ArrayList<String>();
		returnValueDefault = new ArrayList<String>();
		returnValueDefaultType = new ArrayList<Integer>();
	}

	public void getFields(RowMetaInterface row, String name, RowMetaInterface[] info, StepMeta nextStep, VariableSpace space,
			Repository repository, IMetaStore metaStore) throws KettleStepException {

		// Change all the fields to normal storage, this is the fastest way to handle lazy conversion.
		// It doesn't make sense to use it in the SCD context but people try it anyway
		//
		for (ValueMetaInterface valueMeta : row.getValueMetaList()) {
			valueMeta.setStorageType(ValueMetaInterface.STORAGE_TYPE_NORMAL);

			// Also change the trim type to "None" as this can cause trouble during compare
			// of the data when there are leading/trailing spaces in the target table
			//
			valueMeta.setTrimType(ValueMetaInterface.TRIM_TYPE_NONE);
		}

		for (int i = 0; i < returnValueName.size(); i++) {
			ValueMetaInterface v = new ValueMeta(returnValueName.get(i), returnValueDefaultType.get(i));
			if (returnValueNewName.get(i) != null && returnValueNewName.get(i).length() > 0) {
				v.setName(returnValueNewName.get(i));
			}

			v.setLength(9);
			v.setPrecision(0);
			v.setOrigin(name);
			row.addValueMeta(v);
		}
	}

	/**
	 * @return the cassadraHost
	 */
	public String getCassadraHost() {
		return cassadraHost;
	}

	/**
	 * @param cassadraHost
	 *            the cassadraHost to set
	 */
	public void setCassadraHost(String cassadraHost) {
		this.cassadraHost = cassadraHost;
	}

	/**
	 * @return the cassandraPort
	 */
	public String getCassandraPort() {
		return cassandraPort;
	}

	/**
	 * @param cassandraPort
	 *            the cassandraPort to set
	 */
	public void setCassandraPort(String cassandraPort) {
		this.cassandraPort = cassandraPort;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the schemaName
	 */
	public String getKeyspace() {
		return keyspace;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}

	/**
	 * @return the returnValueNewName
	 */
	public List<String> getReturnValueNewName() {
		return returnValueNewName;
	}

	/**
	 * @param returnValueNewName
	 *            the returnValueNewName to set
	 */
	public void setReturnValueNewName(List<String> returnValueNewName) {
		this.returnValueNewName = returnValueNewName;
	}

	/**
	 * @return the returnValueDefault
	 */
	public List<String> getReturnValueDefault() {
		return returnValueDefault;
	}

	/**
	 * @param returnValueDefault
	 *            the returnValueDefault to set
	 */
	public void setReturnValueDefault(List<String> returnValueDefault) {
		this.returnValueDefault = returnValueDefault;
	}

	/**
	 * @return the returnValueDefaultType
	 */
	public List<Integer> getReturnValueDefaultType() {
		return returnValueDefaultType;
	}

	/**
	 * @param returnValueDefaultType
	 *            the returnValueDefaultType to set
	 */
	public void setReturnValueDefaultType(List<Integer> returnValueDefaultType) {
		this.returnValueDefaultType = returnValueDefaultType;
	}

	/**
	 * @return the keyName
	 */
	public List<String> getKeyName() {
		return keyName;
	}

	/**
	 * @param keyName
	 *            the keyName to set
	 */
	public void setKeyName(List<String> keyName) {
		this.keyName = keyName;
	}

	/**
	 * @return the keyCondition
	 */
	public List<String> getKeyCondition() {
		return keyCondition;
	}

	/**
	 * @param keyCondition
	 *            the keyCondition to set
	 */
	public void setKeyCondition(List<String> keyCondition) {
		this.keyCondition = keyCondition;
	}

	/**
	 * @return the keyCompareFieldName
	 */
	public List<String> getKeyCompareFieldName() {
		return keyCompareFieldName;
	}

	/**
	 * @param keyCompareFieldName
	 *            the keyCompareFieldName to set
	 */
	public void setKeyCompareFieldName(List<String> keyCompareFieldName) {
		this.keyCompareFieldName = keyCompareFieldName;
	}

	/**
	 * @return the keyCompareValue
	 */
	public List<String> getKeyCompareValue() {
		return keyCompareValue;
	}

	/**
	 * @param keyCompareValue
	 *            the keyCompareValue to set
	 */
	public void setKeyCompareValue(List<String> keyCompareValue) {
		this.keyCompareValue = keyCompareValue;
	}

	/**
	 * @return the returnValueName
	 */
	public List<String> getReturnValueName() {
		return returnValueName;
	}

	/**
	 * @param returnValueName
	 *            the returnValueName to set
	 */
	public void setReturnValueName(List<String> returnValueName) {
		this.returnValueName = returnValueName;
	}

	// Dummy main so we can test compile in eclipse
	public static void main(String[] args) {

	}

	/**
	 * @return the columnFamily
	 */
	public String getColumnFamily() {
		return columnFamily;
	}

	/**
	 * @param columnFamily
	 *            the columnFamily to set
	 */
	public void setColumnFamily(String columnFamily) {
		this.columnFamily = columnFamily;
	}

	/**
	 * @return the withSSL
	 */
	public Boolean getWithSSL() {
		if (withSSL == null) {
			withSSL = false;
		}
		return withSSL;
	}

	/**
	 * @param withSSL
	 *            the withSSL to set
	 */
	public void setWithSSL(Boolean withSSL) {
		this.withSSL = withSSL;
	}

	/**
	 * @return the trustStoreFilePath
	 */
	public String getTrustStoreFilePath() {
		return trustStoreFilePath;
	}

	/**
	 * @param trustStoreFilePath
	 *            the trustStoreFilePath to set
	 */
	public void setTrustStoreFilePath(String trustStoreFilePath) {
		this.trustStoreFilePath = trustStoreFilePath;
	}

	/**
	 * @return the trustStorePass
	 */
	public String getTrustStorePass() {
		return trustStorePass;
	}

	/**
	 * @param trustStorePass
	 *            the trustStorePass to set
	 */
	public void setTrustStorePass(String trustStorePass) {
		this.trustStorePass = trustStorePass;
	}

	/**
	 * @return the compression
	 */
	public ConnectionCompression getCompression() {
		return compression;
	}

	/**
	 * @param compression
	 *            the compression to set
	 */
	public void setCompression(ConnectionCompression compression) {
		this.compression = compression;
	}

}
