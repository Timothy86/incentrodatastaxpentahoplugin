/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.output;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;

/**
 * Data class for the CassandraOutput step.
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class DatastaxCassandraOutputData extends BaseStepData implements StepDataInterface {

	/** The output data format */
	protected RowMetaInterface m_outputRowMeta;

	/**
	 * Get the output row format
	 * 
	 * @return the output row format
	 */
	public RowMetaInterface getOutputRowMeta() {
		return m_outputRowMeta;
	}

	/**
	 * Set the output row format
	 * 
	 * @param rmi
	 *            the output row format
	 */
	public void setOutputRowMeta(RowMetaInterface rmi) {
		m_outputRowMeta = rmi;
	}
}
