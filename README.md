incentro-cassandra-plugin
=========================
This Pentaho plugin provides access to a Cassandra Cluster using the Datastax drivers.  
It includes three steps:
- Datastax Cassandra Input
- Datastax Cassandra Lookup
- Datastax Cassandra Output

By using the Datastax driver connections can be made to Apache Cassandra 1.2+ or DSE 3.2+
it will use the native connection on port 9042 which can be secured by SSL. If SSL is used
with a self-signed certificate you can provide a java truststore file to make it work.

development
-----------
This plugin was developed for specific needs, new functionality can be requested but it
is better to send a patch or pull-request with the required changes.

step descriptions
-----------------
A short description of the steps

### Datastax Cassandra Input
This step behaves like a normal Table Input, only you have to provide connection details per step.

Not yet implemented
- Get CQL Statement
- lazy conversion
- using parameters from info stream

### Datastax Cassandra Lookup
This step behaves like a normal Database lookup, only you have to provide connection details per step.

Not yet implemented
- caching
- specify handling of multiple results (now: use first row)

### Datastax Cassandra Output
This step behaves like a normal Table Output, only you have to provide connection details per step.

Not yet implemented
- all datatypes (Integer and String works)
